// //Task 1
// let inputStr = prompt('Enter string:');
//
// function isPalindrome(str) {
//     let reversedStr = '';
//     for (let i = str.length - 1; i >= 0; i--) {
//         reversedStr += str[i];
//     }
//
//     return str === reversedStr;
// }
//
// console.log("Task1: ", isPalindrome(inputStr));
//
// //Task 2
// inputStr = prompt('Enter string:');
// let inputStrLength = prompt('Enter string length:');
//
// function isNormalLength(str, inputStrLength) {
//     return str.length <= inputStrLength;
// }
//
// console.log("Task2: ", isNormalLength(inputStr, inputStrLength));

//Task 3
let inputDate = prompt('Enter your birth date in format "dd-MM-yyyy":').split("-");

function calculateAge(inputDate) {
    let date = new Date();
    date.setDate(inputDate[0]);
    date.setMonth(inputDate[1]);
    date.setFullYear(inputDate[2]);
    return new Date().getFullYear() - date.getFullYear();
}

console.log(`Task3: your age is ${calculateAge(inputDate)} years`);