1. Як можна створити рядок у JavaScript?

   У JavaScript рядок можна створити рядок за допомогою одинарних кавичок, подвійних кавичок, шаблонного літералу, та
   при створенні об'єкту String.

2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?

   Різниці між подвійними та одинарними лапками немає. Шаблонний літерал особливий тим, що він може приймати в себе
   значення змінних та не потребує використання спеціального символу '\n'.
   Приклад:
   ```javascript
   const mystr = 'It`s my string:'
   const str = `${mystr}
   Lorem ipsum dolor sit amet.`;
   ```
   
3. Як перевірити, чи два рядки рівні між собою?
   
   Рядки можна перевіряти по їх довжині:
   ```javascript
   const firstStr = "First";
   const secondStr = "Second";
   console.log(firstStr.length < secondStr.length); // true
   ```
   Та посимвольно (за таблицею ASCII):   
   ```javascript
   const firstStr = "First";
   const secondStr = "Second";
   console.log(firstStr < secondStr); // true
   ```

4. Що повертає Date.now()?

   Date.now() повертає поточну дату та час.

5. Чим відрізняється Date.now() від new Date()?

   Повертає к-ть мілісекунд, що пройшли з 01.01.1970 року в UTC.